Schibsted Tech Challenge
==================
Schibsted

# Contents

 * [Requirements](#requirements)
 * [Installation](#installation)
 * [Usage](#usage)
 * [Licence](#licence)

<a name="requirements"/>
# Requirements
Dependencies:

 - Java 8
 - Maven
 - Jtwig
 - Slf4j
 - Guava
 - Gson
 - JUnit
 - Mockito

<a name="installation"/>
# Installation
First we should run (in order to compile the entire project):

    mvn clean install

Once that's done, then we can execute it using maven:

    mvn exec:java

Or, we can also do it by excuting the .jar file from the command line:

    java -cp target/schibsted-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.schibsted.assignment.App

<a name="usage"/>
# Usage
As requested there are two main use cases:

 - By accessing to [the login page](http://localhost:8000/login), in which we can login with the following credentials: 
 
       - user1/pass1
       - user2/pass2
       - user3/pass3
       - admin/admin
     
 - By using the REST API, in this [endpoint](http://localhost:8000/api/user). Beware that it will use Basic Authentication with the admin credentials. So far, it doesn't support Path Params, so if you need to retrieve a single user, you must use this [url](http://localhost:8000/api/user?id=1), for example (do not forget to set the Accept header to 'application/json; charset=utf-8').

<a name="licence"/>
# License

[MIT License](http: //facundofarias.mit-license.org/) © Facundo Farias