package com.schibsted.assignment;

import java.io.IOException;

import com.schibsted.assignment.http.BasicServer;

public class App {

	public static void main(String[] args) throws IOException {
		new BasicServer();
	}

}
