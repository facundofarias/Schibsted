package com.schibsted.assignment.domain;

import java.util.UUID;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class Session {

	private String id;
	private Token token;

	public Session() {
		id = UUID.randomUUID().toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Session other = (Session) obj;
		return Objects.equal(this.id, other.id);
	}

	public String getId() {
		return id;
	}

	public Token getToken() {
		return token;
	}

	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	public boolean hasToken() {
		return token != null;
	}

	public boolean matchesId(String id) {
		return id.equals(id);
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setToken(Token token) {
		this.token = token;
	}
	
	public void createToken(User user)
	{
		token = new Token(user);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("id", id).add("token", token).toString();
	}

}
