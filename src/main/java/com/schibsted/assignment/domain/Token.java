package com.schibsted.assignment.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class Token {
	private static final int SESSION_EXPIRATION_IN_MINUTES = 5;

	private String id;
	private User user;
	private LocalDateTime expiration;

	public Token(User user) {
		this.user = user;
		id = UUID.randomUUID().toString();
		refreshExpiration();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Token other = (Token) obj;
		return Objects.equal(this.id, other.id);
	}

	public LocalDateTime getExpiration() {
		return expiration;
	}

	public String getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	public boolean isExpired() {
		return expiration.isBefore(LocalDateTime.now());
	}

	public boolean matchesId(String id) {
		return id.equals(id);
	}

	public void refreshExpiration() {
		expiration = LocalDateTime.now().plusMinutes(SESSION_EXPIRATION_IN_MINUTES);
	}

	public void setExpiration(LocalDateTime expiration) {
		this.expiration = expiration;
	}

	public void expire() {
		expiration = LocalDateTime.now();
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("id", id).add("user", user).add("expiration", expiration)
				.toString();
	}

}
