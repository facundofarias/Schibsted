package com.schibsted.assignment.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * User domain class.
 */
public class User implements Serializable {
	private static final long serialVersionUID = 7679085449561293562L;

	private int id;
	private String profileUrl;
	private String username;
	private String password;
	private Collection<Role> roles;

	public User() {
		this.roles = new ArrayList<>();
	}

	public User(int id, String username, String password, Role role) {
		this();
		this.id = id;
		this.username = username;
		this.password = password;
		this.addRole(role);
		this.profileUrl = String.format("/profile/page%s", id);
	}

	public void addRole(Role r) {
		roles.add(r);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final User other = (User) obj;
		return Objects.equal(this.id, other.id) && Objects.equal(this.username, other.username)
				&& Objects.equal(this.password, other.password) && Objects.equal(this.roles, other.roles);
	}

	public int getId() {
		return id;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public String getUsername() {
		return username;
	}

	public int hashCode() {
		return Objects.hashCode(this.id, this.username, this.password, this.roles);
	}

	public boolean hasRole(Role role) {
		return roles.contains(role);
	}

	public boolean isAdmin() {
		return hasRole(Role.ADMIN);
	}
	
	public boolean isProfileUrl(String url) {
		return profileUrl.equalsIgnoreCase(url);
	}

	public boolean matchesUserAndPassword(String username, String password) {
		return this.username.equals(username) && this.password.equals(password);
	}

	public boolean matchesId(int id) {
		return this.id == id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("id", id).add("roles", roles).add("username", username).toString();
	}
}
