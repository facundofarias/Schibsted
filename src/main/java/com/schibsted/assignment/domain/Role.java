package com.schibsted.assignment.domain;

/**
 * Role domain enum.
 */
public enum Role
{
    ROL_1,
    ROL_2,
    ROL_3,
    ADMIN
}
