package com.schibsted.assignment.http.handlers;

import java.io.IOException;

import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.http.controllers.Controller;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.HttpExchange;

import sun.net.www.protocol.http.HttpURLConnection;

public class RootHandler extends BaseHandler {

	public RootHandler(AppRegistry registry) {
		super(registry);
	}

	public void handle(HttpExchange exchange) throws IOException {
		String path = exchange.getRequestURI().getPath().toLowerCase();
		try {

			Controller controller = dispatcher.dispatch(path);
			controller.doAction(exchange);

		} catch (NotFoundException e) {
			handleError(exchange, HttpURLConnection.HTTP_NOT_FOUND);
		}
	}
}
