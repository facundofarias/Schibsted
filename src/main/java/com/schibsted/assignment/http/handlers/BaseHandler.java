package com.schibsted.assignment.http.handlers;

import java.io.IOException;
import java.io.OutputStream;

import com.schibsted.assignment.http.ControllerDispatcher;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

abstract class BaseHandler implements HttpHandler {
	
	final ControllerDispatcher dispatcher;

	BaseHandler(AppRegistry registry) {
		dispatcher = new ControllerDispatcher(registry);
	}
	
	void handleError(HttpExchange exchange, int httpStatus) throws IOException {
		String response = String.format("Ups! This an HTTP ERROR: %s.", httpStatus);
		exchange.sendResponseHeaders(httpStatus, response.length());
		OutputStream os = exchange.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}
}
