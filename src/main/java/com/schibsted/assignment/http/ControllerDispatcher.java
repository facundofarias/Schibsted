package com.schibsted.assignment.http;

import java.util.HashMap;
import java.util.Map;

import com.schibsted.assignment.domain.User;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.http.controllers.Controller;
import com.schibsted.assignment.http.controllers.api.UserController;
import com.schibsted.assignment.http.controllers.web.LoginController;
import com.schibsted.assignment.http.controllers.web.LoginDoController;
import com.schibsted.assignment.http.controllers.web.LogoutController;
import com.schibsted.assignment.http.controllers.web.ProfileController;
import com.schibsted.assignment.registry.AppRegistry;

public class ControllerDispatcher {
	private final AppRegistry registry;
	private final Map<String, Controller> routes;

	public ControllerDispatcher(AppRegistry registry) {
		this.registry = registry;
		this.routes = new HashMap<>();
		loadRoutes();
	}

	public Controller dispatch(String path) throws NotFoundException {

		System.out.format("Routing path: %s\n", path);
		
		if(routes.containsKey(path)) {
			return routes.get(path);
		} else {
			throw new NotFoundException(String.format("Route %s wasn't found.\n", path));
		}
	}

	private void loadRoutes() {
		routes.put("/login", new LoginController(registry));
		routes.put("/login/do", new LoginDoController(registry));
		routes.put("/logout", new LogoutController(registry));
		routes.put("/api/user", new UserController(registry));

		for (User user : registry.getAllUsers()) {
			routes.put(user.getProfileUrl(), new ProfileController(registry));
		}
	}
}
