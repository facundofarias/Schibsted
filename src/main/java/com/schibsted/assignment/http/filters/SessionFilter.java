package com.schibsted.assignment.http.filters;

import java.io.IOException;
import java.util.Collection;

import com.google.common.net.HttpHeaders;
import com.schibsted.assignment.Constants;
import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;

public class SessionFilter extends Filter {
	private final AppRegistry registry;

	public SessionFilter(AppRegistry registry) {
		this.registry = registry;
	}

	@Override
	public String description() {
		return "Set the user session";
	}

	@Override
	public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
		String sessionId = getCookie(httpExchange, Constants.SESSION_TOKEN_CONSTANT);
		Session session = findOrCreateSession(sessionId);
		addCookie(httpExchange, Constants.SESSION_TOKEN_CONSTANT, session.getId());
		httpExchange.setAttribute(Constants.SESSION_CONSTANT, session);
		chain.doFilter(httpExchange);
	}
	
	private void addCookie(HttpExchange httpExchange, String key, String value) {
		httpExchange.getResponseHeaders().add(HttpHeaders.SET_COOKIE, key + "=" + value + ";Path=/;");
	}

	private String getCookie(HttpExchange httpExchange, String key) {
		Collection<String> cookiesList = httpExchange.getRequestHeaders().get(HttpHeaders.COOKIE);
		if (cookiesList == null) {
			return "";
		}
		for (String cookies : cookiesList) {
			String[] cookiesArray = cookies.split(";");
			for (String cookieInfo : cookiesArray) {
				String[] cookie = cookieInfo.split("=");
				if (key.equals(cookie[0].trim()) && cookie.length == 2) {
					return cookie[1];
				}
			}
		}
		return "";
	}

	private Session findOrCreateSession(String sessionId) {
		try {

			return registry.findSessionById(sessionId);

		} catch (NotFoundException e) {
			Session newSession = new Session();
			registry.save(newSession);
			return newSession;
		}
	}

}
