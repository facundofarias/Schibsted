package com.schibsted.assignment.http.filters;

import java.io.IOException;

import com.google.common.base.Strings;
import com.google.common.net.HttpHeaders;
import com.schibsted.assignment.Constants;
import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.domain.Token;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.exceptions.SessionExpiredException;
import com.schibsted.assignment.exceptions.UnauthorizedException;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;

import sun.net.www.protocol.http.HttpURLConnection;

public class AuthenticationFilter extends Filter {

	@Override
	public String description() {
		return "Verify user authentication";
	}

	@Override
	public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
		String path = httpExchange.getRequestURI().getPath().toLowerCase();
		if (requiresAuthorization(path)) {
			checkAuthorization(httpExchange, path);
		}
		chain.doFilter(httpExchange);
	}
	
	private void checkAuthorization(HttpExchange httpExchange, String path) throws IOException {
		try {
			Token token = retrieveToken(httpExchange);
			if (!token.getUser().isProfileUrl(path)) {
				throw new UnauthorizedException(String.format("User is not authorized on path %s.", path));
			}
		} catch (NotFoundException e) {
			handleError(httpExchange, HttpURLConnection.HTTP_MOVED_TEMP, "/login?PleaseLogIn");

		} catch (SessionExpiredException e) {
			handleError(httpExchange, HttpURLConnection.HTTP_MOVED_TEMP, "/login?SessionExpired");

		} catch (UnauthorizedException e) {
			handleError(httpExchange, HttpURLConnection.HTTP_FORBIDDEN, null);
		}
	}

	private void handleError(HttpExchange httpExchange, int httpStatus, String newUrl) throws IOException {

		if (!Strings.isNullOrEmpty(newUrl)) {
			httpExchange.getResponseHeaders().add(HttpHeaders.LOCATION, newUrl);
		}
		httpExchange.sendResponseHeaders(httpStatus, -1);
	}

	private boolean requiresAuthorization(String path) {
		return !path.contains("login") && !path.contains("logout");
	}

	private Token retrieveToken(HttpExchange httpExchange) throws SessionExpiredException, NotFoundException {
		Session session = (Session) httpExchange.getAttribute(Constants.SESSION_CONSTANT);
		if (!session.hasToken()) {
			throw new NotFoundException(String.format("Session %s has no token.", session.getId()));
		}

		Token token = session.getToken();
		if (token.isExpired()) {
			throw new SessionExpiredException(String.format("Token %s has expired.", token.getId()));
		}

		token.refreshExpiration();
		return token;
	}

}
