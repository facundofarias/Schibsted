package com.schibsted.assignment.http.controllers.web;

import java.io.IOException;

import com.google.common.net.HttpHeaders;
import com.schibsted.assignment.Constants;
import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.http.controllers.BaseController;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.HttpExchange;

public class LogoutController extends BaseController
{
	private static final String EXPIRE_COOKIE_TEMPLATE = "sessionToken=%s;Path=/;expires=Thu, 01 Jan 1970 00:00:00 GMT";

	public LogoutController(AppRegistry registry)
	{
		super(registry);
	}
	
	@Override
	public void doAction(HttpExchange exchange) throws IOException {
		
		Session session = (Session) exchange.getAttribute(Constants.SESSION_CONSTANT);
		if(session.hasToken()) {
			session.getToken().expire();
			exchange.getResponseHeaders().add(HttpHeaders.SET_COOKIE, String.format(EXPIRE_COOKIE_TEMPLATE, session.getId()));
		}
		registry.remove(session);
		redirect(exchange, "/login?PleaseLogIn");
	}
}
