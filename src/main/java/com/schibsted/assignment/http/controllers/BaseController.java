package com.schibsted.assignment.http.controllers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Maps;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.domain.Token;
import com.schibsted.assignment.http.views.ViewComposer;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import static com.google.common.net.HttpHeaders.ACCEPT;
import static com.google.common.net.HttpHeaders.CONTENT_TYPE;
import static java.net.HttpURLConnection.HTTP_MOVED_TEMP;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNSUPPORTED_TYPE;

public abstract class BaseController implements Controller {
	private static final String PARAMS_CONSTANT = "params";
	private static final String SESSION_CONSTANT = "session";

	protected final AppRegistry registry;
	private final ViewComposer composer;

	public BaseController(AppRegistry registry) {
		this.registry = registry;
		this.composer = new ViewComposer();
	}

	protected String getParameter(HttpExchange exchange, String parameterName) throws IOException {
		Map<?, ?> parameters = (Map<?, ?>) exchange.getAttribute(PARAMS_CONSTANT);
		return parameters.containsKey(parameterName) ? (String) parameters.get(parameterName) : "";
	}

	protected Map<String, String> getSessionParameters(HttpExchange exchange) {
		Session session = (Session) exchange.getAttribute(SESSION_CONSTANT);
		Map<String, String> params = new HashMap<>();
		Token token = session.getToken();
		params.put("username", token.getUser().getUsername());
		return params;
	}

	protected void redirect(HttpExchange exchange, String newUrl) throws IOException {
		exchange.getResponseHeaders().add(HttpHeaders.LOCATION, newUrl);
		exchange.sendResponseHeaders(HTTP_MOVED_TEMP, -1);
	}

	protected void showView(HttpExchange exchange, String viewName) throws IOException {
		showView(exchange, viewName, Maps.newHashMap());
	}

	protected void showView(HttpExchange exchange, String viewName, Map<String, String> params) throws IOException {
		String response = composer.renderView(viewName, params);
		exchange.getResponseHeaders().set(CONTENT_TYPE, MediaType.HTML_UTF_8.toString());
		exchange.sendResponseHeaders(HTTP_OK, response.length());
		OutputStream os = exchange.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}

    protected void sendResponse(HttpExchange exchange, MediaType mediaType, String response) throws IOException {
        exchange.getResponseHeaders().set(CONTENT_TYPE, mediaType.toString());
        exchange.sendResponseHeaders(HTTP_OK, response.length());
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    protected boolean isMediaTypeSupported(HttpExchange httpExchange) throws IOException
    {
        if(!isJsonMediaTypeAccepted(httpExchange.getRequestHeaders())) {
            httpExchange.sendResponseHeaders(HTTP_UNSUPPORTED_TYPE, -1);
            return false;
        }
        return true;
    }

    protected boolean isMediaTypeJson(HttpExchange httpExchange) throws IOException
    {
        if(!isJsonMediaTypeJson(httpExchange.getRequestHeaders())) {
            httpExchange.sendResponseHeaders(HTTP_UNSUPPORTED_TYPE, -1);
            return false;
        }
        return true;
    }

    private boolean isJsonMediaTypeAccepted(Headers requestHeaders)
    {
        return requestHeaders.containsKey(ACCEPT) &&
                requestHeaders.get(ACCEPT).contains(MediaType.JSON_UTF_8.toString());
    }

    private boolean isJsonMediaTypeJson(Headers requestHeaders)
    {
        return requestHeaders.containsKey(CONTENT_TYPE) &&
                requestHeaders.get(CONTENT_TYPE).contains(MediaType.JSON_UTF_8.toString());
    }
}
