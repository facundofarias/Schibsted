package com.schibsted.assignment.http.controllers;

import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;

/**
 * Generic controller interface
 */
public interface Controller {
	
	/**
	 * Perform controller specific action
	 */
	void doAction(HttpExchange exchange) throws IOException;

}
