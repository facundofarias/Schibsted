package com.schibsted.assignment.http.controllers.api;

import com.google.common.base.Strings;
import com.google.common.net.MediaType;
import com.google.gson.Gson;
import com.schibsted.assignment.domain.User;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.http.controllers.BaseController;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.HttpExchange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static java.net.HttpURLConnection.HTTP_BAD_METHOD;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;

public class UserController extends BaseController {
	private static String read(InputStream input) throws IOException {
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
			return buffer.lines().collect(Collectors.joining("\n"));
		}
	}

	private final Gson gson;

	public UserController(AppRegistry registry) {
		super(registry);
		gson = new Gson();
	}

	@Override
	public void doAction(HttpExchange httpExchange) throws IOException {
		String verb = httpExchange.getRequestMethod().toUpperCase();

		switch (verb) {
		case "GET":
			doGet(httpExchange);
			break;
		case "POST":
			doPost(httpExchange);
			break;
		case "PUT":
			doPut(httpExchange);
			break;
		case "DELETE":
			doDelete(httpExchange);
			break;
		default:
			httpExchange.sendResponseHeaders(HTTP_BAD_METHOD, -1);
			break;
		}

	}

	protected void doDelete(HttpExchange httpExchange) throws IOException {
		String idParam = getParameter(httpExchange, "id");
		if (!Strings.isNullOrEmpty(idParam)) {
			try {
				int id = Integer.valueOf(idParam);
				User user = registry.findUserById(id);
				registry.remove(user);
				httpExchange.sendResponseHeaders(HTTP_OK, -1);

			} catch (NotFoundException | NumberFormatException e) {
				httpExchange.sendResponseHeaders(HTTP_NOT_FOUND, -1);
			}

		} else {
			httpExchange.sendResponseHeaders(HTTP_NOT_FOUND, -1);
		}
	}

	protected void doGet(HttpExchange httpExchange) throws IOException {
		if (!isMediaTypeSupported(httpExchange)) {
			return;
		}

		String paramId = getParameter(httpExchange, "id");
		String response;
		if (Strings.isNullOrEmpty(paramId)) {

			response = gson.toJson(registry.getAllUsers());
		} else {

			try {
				int id = Integer.valueOf(paramId);
				response = gson.toJson(registry.findUserById(id));
				
			} catch (NotFoundException | NumberFormatException e) {
				httpExchange.sendResponseHeaders(HTTP_NOT_FOUND, -1);
				return;
			}
		}

		sendResponse(httpExchange, MediaType.JSON_UTF_8, response);
	}

	protected void doPost(HttpExchange httpExchange) throws IOException {
		addOrReplaceUser(httpExchange);
	}

	protected void doPut(HttpExchange httpExchange) throws IOException {
		addOrReplaceUser(httpExchange);
	}
	
	private void addOrReplaceUser(HttpExchange httpExchange) throws IOException {
		if (isMediaTypeJson(httpExchange)) {
			String requestBody = read(httpExchange.getRequestBody());
			User user = gson.fromJson(requestBody, User.class);
			registry.save(user);
			httpExchange.sendResponseHeaders(HTTP_OK, -1);
		}
	}
}
