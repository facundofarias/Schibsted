package com.schibsted.assignment.http.controllers.web;

import java.io.IOException;

import com.schibsted.assignment.http.controllers.BaseController;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.HttpExchange;

public class LoginController extends BaseController
{
	public LoginController(AppRegistry registry) {
		super(registry);
	}

	@Override
	public void doAction(HttpExchange exchange) throws IOException {

		showView(exchange, "login");
	}
}