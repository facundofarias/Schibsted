package com.schibsted.assignment.http.controllers.web;

import java.io.IOException;

import com.schibsted.assignment.http.controllers.BaseController;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.HttpExchange;

public class ProfileController extends BaseController
{
	public ProfileController(AppRegistry registry)
	{
		super(registry);
	}

	@Override
	public void doAction(HttpExchange exchange) throws IOException {
		
		showView(exchange, "profile", getSessionParameters(exchange));
	}
}
