package com.schibsted.assignment.http.controllers.web;

import java.io.IOException;

import com.schibsted.assignment.Constants;
import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.domain.User;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.http.controllers.BaseController;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.HttpExchange;

public class LoginDoController extends BaseController
{
	public LoginDoController(AppRegistry registry)
	{
		super(registry);
	}

	@Override
	public void doAction(HttpExchange exchange) throws IOException {
		
		String username = getParameter(exchange, "username");
		String password = getParameter(exchange, "password");
		
		try {
			User user = registry.findUser(username, password);
			System.out.format("The user %s was successfully found!\n", user.getUsername());
			
	        Session session = (Session) exchange.getAttribute(Constants.SESSION_CONSTANT);
	        session.createToken(user);
			redirect(exchange, user.getProfileUrl());
			
		} catch (NotFoundException e) {
			System.out.format("The user %s wasn't found!\n", username);
			redirect(exchange, "/login?Ups");
		}
	}
}
