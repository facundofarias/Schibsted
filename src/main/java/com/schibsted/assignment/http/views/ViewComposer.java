package com.schibsted.assignment.http.views;

import java.util.Map;
import java.util.Map.Entry;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

public class ViewComposer {

	public String renderView(String viewName, Map<String, String> params) {

		String viewTemplate = String.format("templates/%s.twig", viewName);
		JtwigTemplate template = JtwigTemplate.classpathTemplate(viewTemplate);
		JtwigModel model = JtwigModel.newModel();
		
		for (Entry<String, String> param : params.entrySet()) {
			model.with(param.getKey(), param.getValue());
		}
		
		return template.render(model);
	}
}
