package com.schibsted.assignment.http.security;

import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.registry.AppRegistry;
import com.sun.net.httpserver.BasicAuthenticator;

public class ApiAuthenticator extends BasicAuthenticator{
	private AppRegistry registry;
	
	public ApiAuthenticator(String realm, AppRegistry registry)
	{
		super(realm);
		this.registry = registry;
	}

	@Override
	public boolean checkCredentials(String username, String password) {
		try {
			
			return registry.findUser(username, password).isAdmin();
			
		} catch (NotFoundException e) {
			System.out.format("User %s wasn't found!\n", username);
			return false;
		}
	}
}
