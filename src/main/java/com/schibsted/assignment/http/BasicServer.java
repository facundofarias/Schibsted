package com.schibsted.assignment.http;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import com.schibsted.assignment.http.filters.AuthenticationFilter;
import com.schibsted.assignment.http.filters.ParamFilter;
import com.schibsted.assignment.http.filters.SessionFilter;
import com.schibsted.assignment.http.handlers.RootHandler;
import com.schibsted.assignment.http.security.ApiAuthenticator;
import com.schibsted.assignment.registry.AppRegistry;
import com.schibsted.assignment.registry.DefaultAppRegistry;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

public class BasicServer {
	private static final String API_PATH = "/api";
	private static final String ROOT_PATH = "/";
	
	private HttpServer server;
	private AppRegistry registry = new DefaultAppRegistry();

	public BasicServer(int port) throws IOException {
		server = HttpServer.create(new InetSocketAddress(port), 0);
		
		HttpContext apiContext = server.createContext(API_PATH, new RootHandler(registry));
		apiContext.getFilters().add(new ParamFilter());
		apiContext.setAuthenticator(new ApiAuthenticator("get", registry));
		
		HttpContext webContext = server.createContext(ROOT_PATH, new RootHandler(registry));
		webContext.getFilters().add(new ParamFilter());
		webContext.getFilters().add(new SessionFilter(registry));
		webContext.getFilters().add(new AuthenticationFilter());
		
		server.setExecutor(Executors.newCachedThreadPool());
		server.start();
		
		System.out.format("Started server at: %s\n", server.getAddress());
	}
	
	public BasicServer() throws IOException
	{
		this(8000);
	}

}
