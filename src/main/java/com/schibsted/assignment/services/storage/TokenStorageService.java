package com.schibsted.assignment.services.storage;

import com.schibsted.assignment.domain.Token;
import com.schibsted.assignment.exceptions.NotFoundException;

/**
 * Token Storage Service
 */
public interface TokenStorageService {

	/**
	 * Returns a given token by its id.
	 *
	 * @param id
	 *            the token's id
	 * @return the token
	 * @throws NotFoundException 
	 */
	Token findTokenById(String id) throws NotFoundException;

	/**
	 * Saves a given token into Storage.
	 *
	 * @param the
	 *            token
	 */
	void save(Token token);

	/**
	 * Removes a given token from Storage.
	 *
	 * @param the
	 *            token
	 */
	void remove(Token token);
}
