package com.schibsted.assignment.services.storage;

import java.util.Collection;

import com.schibsted.assignment.domain.User;
import com.schibsted.assignment.exceptions.NotFoundException;

/**
 * User Storage Service
 */
public interface UserStorageService {
	/**
	 * Returns a given User if he can be found on the storage.
	 *
	 * @param username
	 *            the username to find
	 * @param password
	 *            the username's password
	 * @return the given user
	 */
	User findUser(String username, String password) throws NotFoundException;

	/**
	 * Returns a given user by its id.
	 *
	 * @param id
	 *            the user's id
	 * @return the user
	 * @throws NotFoundException
	 */
	User findUserById(int id) throws NotFoundException;

	/**
	 * Returns the user list.
	 *
	 * @return the given user's collection
	 */
	Collection<User> getAllUsers();

	/**
	 * Removes a given user from Storage.
	 *
	 * @param user
	 *            the user to remove
	 */
	void remove(User user);

	/**
	 * Saves a given user into Storage.
	 *
	 * @param user
	 *            the user to save
	 */
	void save(User user);
}
