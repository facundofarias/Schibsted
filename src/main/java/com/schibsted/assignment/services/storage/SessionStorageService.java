package com.schibsted.assignment.services.storage;

import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.exceptions.NotFoundException;

/**
 * Session Storage Service
 */
public interface SessionStorageService {
	
	/**
	 * Returns a given session by its id.
	 *
	 * @param id
	 *            the token's id
	 * @return the session
	 * @throws NotFoundException 
	 */
	Session findSessionById(String id) throws NotFoundException;

	/**
	 * Saves a given session into Storage.
	 *
	 * @param the
	 *            session to save
	 */
	void save(Session session);

	/**
	 * Removes a given token from Storage.
	 *
	 * @param the
	 *            session to remove
	 */
	void remove(Session session);

}
