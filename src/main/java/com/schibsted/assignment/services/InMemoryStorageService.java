package com.schibsted.assignment.services;

import java.util.Collection;
import java.util.HashSet;

import com.schibsted.assignment.domain.Role;
import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.domain.Token;
import com.schibsted.assignment.domain.User;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.services.storage.SessionStorageService;
import com.schibsted.assignment.services.storage.TokenStorageService;
import com.schibsted.assignment.services.storage.UserStorageService;

public class InMemoryStorageService implements UserStorageService, TokenStorageService, SessionStorageService {

	private Collection<Session> sessions;
	private Collection<Token> tokens;
	private Collection<User> users;

	public InMemoryStorageService() {
		users = new HashSet<>();
		tokens = new HashSet<>();
		sessions = new HashSet<>();
		loadTestData();
	}

	@Override
	public Session findSessionById(String id) throws NotFoundException {
		for (Session session : sessions) {
			if (session.matchesId(id)) {
				return session;
			}
		}

		throw new NotFoundException(String.format("The session %s cannot be found.", id));
	}

	@Override
	public Token findTokenById(String id) throws NotFoundException {
		for (Token token : tokens) {
			if (token.matchesId(id)) {
				return token;
			}
		}

		throw new NotFoundException(String.format("The token %s cannot be found.", id));
	}

	@Override
	public User findUser(String username, String password) throws NotFoundException {

		for (User user : users) {
			if (user.matchesUserAndPassword(username, password)) {
				return user;
			}
		}

		throw new NotFoundException(String.format("The user %s cannot be found.", username));
	}

	@Override
	public User findUserById(int id) throws NotFoundException {
		for (User user : users) {
			if (user.matchesId(id)) {
				return user;
			}
		}

		throw new NotFoundException(String.format("The user %s cannot be found.", id));
	}

	@Override
	public Collection<User> getAllUsers() {
		return users;
	}

	private void loadTestData() {
		User user1 = new User(1, "user1", "pass1", Role.ROL_1);
		users.add(user1);

		User user2 = new User(2, "user2", "pass2", Role.ROL_2);
		users.add(user2);

		User user3 = new User(3, "user3", "pass3", Role.ROL_3);
		users.add(user3);

		User admin = new User(4, "admin", "admin", Role.ADMIN);
		users.add(admin);
	}

	@Override
	public void remove(Session session) {
		sessions.remove(session);
	}

	@Override
	public void remove(Token token) {
		tokens.remove(token);
	}

	@Override
	public void remove(User user) {
		users.remove(user);
	}

	@Override
	public void save(Session session) {
		sessions.add(session);
	}

	@Override
	public void save(Token token) {
		tokens.add(token);
	}

	@Override
	public void save(User user) {
		users.add(user);
	}
}
