package com.schibsted.assignment.exceptions;

/**
 * Specific exception to be thrown when the session has expired.
 */
public class SessionExpiredException extends Exception {
	private static final long serialVersionUID = -6875502309133901362L;

	public SessionExpiredException(String message) {
        super(message);
    }
}
