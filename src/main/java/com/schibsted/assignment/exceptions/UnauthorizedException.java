package com.schibsted.assignment.exceptions;

/**
 * Specific exception to be thrown when the access is not authorized.
 */
public class UnauthorizedException extends Exception {
	private static final long serialVersionUID = -6875502309133901362L;

	public UnauthorizedException(String message) {
        super(message);
    }
}
