package com.schibsted.assignment.exceptions;

/**
 * Specific exception to be thrown when something cannot be found.
 */
public class NotFoundException extends Exception {
	private static final long serialVersionUID = -4301093142169603457L;

	public NotFoundException(String message) {
        super(message);
    }
}
