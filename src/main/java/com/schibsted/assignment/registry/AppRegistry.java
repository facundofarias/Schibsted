package com.schibsted.assignment.registry;

import com.schibsted.assignment.services.storage.SessionStorageService;
import com.schibsted.assignment.services.storage.TokenStorageService;
import com.schibsted.assignment.services.storage.UserStorageService;

/**
 * Following Fowler's Registry Pattern (http://martinfowler.com/eaaCatalog/registry.html)
 */
public interface AppRegistry extends UserStorageService, SessionStorageService, TokenStorageService {

}
