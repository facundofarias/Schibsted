package com.schibsted.assignment.registry;

import java.util.Collection;

import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.domain.Token;
import com.schibsted.assignment.domain.User;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.services.InMemoryStorageService;

public class DefaultAppRegistry implements AppRegistry {

	private InMemoryStorageService storageService = new InMemoryStorageService();

	@Override
	public Session findSessionById(String id) throws NotFoundException {
		return storageService.findSessionById(id);
	}

	@Override
	public Token findTokenById(String id) throws NotFoundException {
		return storageService.findTokenById(id);
	}

	@Override
	public User findUser(String username, String password) throws NotFoundException {
		return storageService.findUser(username, password);
	}

	@Override
	public User findUserById(int id) throws NotFoundException {
		return storageService.findUserById(id);
	}

	@Override
	public Collection<User> getAllUsers() {
		return storageService.getAllUsers();
	}

	@Override
	public void remove(Session session) {
		storageService.remove(session);
	}

	@Override
	public void remove(Token token) {
		storageService.remove(token);
	}

	@Override
	public void remove(User user) {
		storageService.remove(user);
	}

	@Override
	public void save(Session session) {
		storageService.save(session);
	}

	@Override
	public void save(Token token) {
		storageService.save(token);
	}

	@Override
	public void save(User user) {
		storageService.save(user);
	}
}
