package com.schibsted.assignment;

public class Constants {
	public static final String SESSION_CONSTANT = "session";
	public static final String SESSION_TOKEN_CONSTANT = "sessionToken";
	
	private Constants() {
		
	}

}
