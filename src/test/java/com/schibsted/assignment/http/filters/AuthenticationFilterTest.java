package com.schibsted.assignment.http.filters;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.sun.net.httpserver.Filter.Chain;

import sun.net.www.protocol.http.HttpURLConnection;

import com.schibsted.assignment.Constants;
import com.schibsted.assignment.domain.Role;
import com.schibsted.assignment.domain.Session;
import com.schibsted.assignment.domain.User;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.Headers;

public class AuthenticationFilterTest {

	@Mock
	private HttpExchange httpExchange;
	
	@Mock
	private Chain chain;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetLoginRemainsUnFiltered() throws IOException {
		
		// Given
		String loginUrl = "/login";
		AuthenticationFilter filter = new AuthenticationFilter();
		given(httpExchange.getRequestURI()).willReturn(URI.create(loginUrl));
		
		// When
		filter.doFilter(httpExchange, chain);
		
		// Then
		assertEquals(URI.create(loginUrl), httpExchange.getRequestURI());
	}
	
	@Test
	public void testGetAnonymousProfile() throws IOException {
		
		// Given
		String loginUrl = "/profile/page1";
		AuthenticationFilter filter = new AuthenticationFilter();
		given(httpExchange.getRequestURI()).willReturn(URI.create(loginUrl));
		given(httpExchange.getAttribute(Constants.SESSION_CONSTANT)).willReturn(new Session());
		given(httpExchange.getResponseHeaders()).willReturn(new Headers());
		
		// When
		filter.doFilter(httpExchange, chain);
		
		// Then
		Mockito.verify(httpExchange, Mockito.times(1))
		.sendResponseHeaders(HttpURLConnection.HTTP_MOVED_TEMP, -1);
		Headers headers = httpExchange.getResponseHeaders();
		assertFalse(headers.isEmpty());
		assertTrue(headers.get("Location").contains("/login?PleaseLogIn"));
	}
	
	@Test
	public void testGetProfileWithLoggedUser() throws IOException {
		
		// Given
		String profileUrl = "/profile/page1";
		User user = new User(1, "test", "pass", Role.ROL_1);
		Session session = new Session();
		session.createToken(user);
		AuthenticationFilter filter = new AuthenticationFilter();
		given(httpExchange.getRequestURI()).willReturn(URI.create(profileUrl));
		given(httpExchange.getAttribute(Constants.SESSION_CONSTANT)).willReturn(session);
		given(httpExchange.getResponseHeaders()).willReturn(new Headers());
		
		// When
		filter.doFilter(httpExchange, chain);
		
		// Then
		assertEquals(URI.create(profileUrl), httpExchange.getRequestURI());
	}
	
	@Test
	public void testGetProfileWithLoggedUserButExpiredToken() throws IOException {
		
		// Given
		String profileUrl = "/profile/page1";
		User user = new User(1, "test", "pass", Role.ROL_1);
		Session session = new Session();
		session.createToken(user);
		session.getToken().setExpiration(LocalDateTime.of(2016, 9, 1, 12, 0));
		
		AuthenticationFilter filter = new AuthenticationFilter();
		given(httpExchange.getRequestURI()).willReturn(URI.create(profileUrl));
		given(httpExchange.getAttribute(Constants.SESSION_CONSTANT)).willReturn(session);
		given(httpExchange.getResponseHeaders()).willReturn(new Headers());
		
		// When
		filter.doFilter(httpExchange, chain);
		
		// Then
		Mockito.verify(httpExchange, Mockito.times(1))
			.sendResponseHeaders(HttpURLConnection.HTTP_MOVED_TEMP, -1);
		Headers headers = httpExchange.getResponseHeaders();
		assertFalse(headers.isEmpty());
		assertTrue(headers.get("Location").contains("/login?SessionExpired"));
	}
	
	@Test
	public void testGetProfileWithLoggedUserWrongContext() throws IOException {
		
		// Given
		String profileUrl = "/profile/page2";
		User user = new User(1, "test", "pass", Role.ROL_1);
		Session session = new Session();
		session.createToken(user);
		AuthenticationFilter filter = new AuthenticationFilter();
		given(httpExchange.getRequestURI()).willReturn(URI.create(profileUrl));
		given(httpExchange.getAttribute(Constants.SESSION_CONSTANT)).willReturn(session);
		given(httpExchange.getResponseHeaders()).willReturn(new Headers());
		
		// When
		filter.doFilter(httpExchange, chain);
		
		// Then
		Mockito.verify(httpExchange, Mockito.times(1))
			.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, -1);
	}

}
