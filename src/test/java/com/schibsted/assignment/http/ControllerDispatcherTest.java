package com.schibsted.assignment.http;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.schibsted.assignment.domain.Role;
import com.schibsted.assignment.domain.User;
import com.schibsted.assignment.exceptions.NotFoundException;
import com.schibsted.assignment.http.controllers.Controller;
import com.schibsted.assignment.http.controllers.web.LoginController;
import com.schibsted.assignment.http.controllers.web.LoginDoController;
import com.schibsted.assignment.http.controllers.web.LogoutController;
import com.schibsted.assignment.http.controllers.web.ProfileController;
import com.schibsted.assignment.registry.AppRegistry;

public class ControllerDispatcherTest {

	@Mock
	private AppRegistry registry;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
		when(registry.getAllUsers()).thenReturn(createMockUsers(4));
	}

	@Test(expected=NotFoundException.class)
	public void testHomePathDispatcher() throws NotFoundException {
		// Given
		String path = "/home";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		dispatcher.dispatch(path);

		// Then
		// NotFoundException is expected
	}

	@Test
	public void testLoginDoPathDispatcher() throws NotFoundException {
		// Given
		String path = "/login/do";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		Controller controller = dispatcher.dispatch(path);

		// Then
		assertNotNull(controller);
		assertTrue(controller instanceof LoginDoController);
	}

	@Test
	public void testLoginPathDispatcher() throws NotFoundException {
		// Given
		String path = "/login";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		Controller controller = dispatcher.dispatch(path);

		// Then
		assertNotNull(controller);
		assertTrue(controller instanceof LoginController);
	}

	@Test
	public void testLogoutPathDispatcher() throws NotFoundException {
		// Given
		String path = "/logout";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		Controller controller = dispatcher.dispatch(path);

		// Then
		assertNotNull(controller);
		assertTrue(controller instanceof LogoutController);
	}

	@Test
	public void testPage1PathDispatcher() throws NotFoundException {
		// Given
		String path = "/profile/page1";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		Controller controller = dispatcher.dispatch(path);

		// Then
		assertNotNull(controller);
		assertTrue(controller instanceof ProfileController);
	}

	@Test
	public void testPage2PathDispatcher() throws NotFoundException {
		// Given
		String path = "/profile/page2";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		Controller controller = dispatcher.dispatch(path);

		// Then
		assertNotNull(controller);
		assertTrue(controller instanceof ProfileController);
	}

	@Test
	public void testPage3PathDispatcher() throws NotFoundException {
		// Given
		String path = "/profile/page3";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		Controller controller = dispatcher.dispatch(path);

		// Then
		assertNotNull(controller);
		assertTrue(controller instanceof ProfileController);
	}

	@Test
	public void testPage4PathDispatcher() throws NotFoundException {
		// Given
		String path = "/profile/page4";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		// When
		Controller controller = dispatcher.dispatch(path);

		// Then
		assertNotNull(controller);
		assertTrue(controller instanceof ProfileController);
	}
	
	@Test(expected=NotFoundException.class)
	public void testPage99PathDispatcher() throws NotFoundException {
		// Given
		String path = "/profile/page99";
		ControllerDispatcher dispatcher = new ControllerDispatcher(registry);

		// When
		dispatcher.dispatch(path);

		// Then
		// An exception is expected
	}
	
	private Collection<User> createMockUsers(int users) {
		Collection<User> usersList = new ArrayList<>();
		for (int i = 1; i <= users; i++) {
			usersList.add(new User(i, "testUser", "testPass", Role.ROL_1));
		}
		return usersList;
	}
}
